# rosbot_control

## Dependencies
```
$ sudo apt-get install ros-melodic-joy*
```

## Starting the program
Please start the rosbot program & the joy node first

```
$ rosrun joy joy_node
```
Make sure that there is no error after starting the node. If there is an error, it might be because of different port configuraton.
Check which port you are using in /dev/input

```
$ cd /dev/input
$ ls
```
Make sure the is js0. If there's another js (js1 for example), change the port by using the command

```
$ rosparam set joy_node/dev "/dev/input/js1"
$ rosrun joy joy_node
```

After the joy_node has started, start the joy_cps_controller.py
```
$ python3 joy_cps_controller.py
```

There are two modes that you can choose:
1. Altogether: This means you can control all the robots at the same time
2. Robot: This means that you control only one robot. To choose which robot you want to control, press the "circle" button

To change between the two modes, use the "triangle" button. You can use the left stick to accelerate/decelerate & right stick to turn
