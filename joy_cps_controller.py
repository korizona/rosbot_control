#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 22:22:03 2021
@author: mason
"""

''' import libraries '''
import time
import numpy as np

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

import sys
import signal

def signal_handler(signal, frame): # ctrl + c -> exit program
        print('You pressed Ctrl+C!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


''' class '''
class robot():
    def __init__(self):
        rospy.init_node('robot_controller', anonymous=True)
        self.max_lin_v = rospy.get_param('/max_linear_vel', 0.7)
        self.max_ang_v = rospy.get_param('/max_angular_vel', 0.7)
        self.robot_number = rospy.get_param('/robot_number', 3)
        self.jackal_robot_number = rospy.get_param('/jackal_robot_number', 2)
        self.input_pub = []
        for i in range(self.robot_number+self.jackal_robot_number):
            if i<self.robot_number:
                self.input_pub.append(rospy.Publisher('/robot'+str(i+1)+'/cmd_vel', Twist, queue_size=10))
            else:
                self.input_pub.append(rospy.Publisher('/robot'+str(i+1)+'/jackal_velocity_controller/cmd_vel', Twist, queue_size=10))
        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_callback)

        self.rate = rospy.Rate(100)
        self.joy_check = 0
        self.hold = 0 #to pause using joystick
        self.number = 0
        self.together = 0
        self.all_stop = 0

    def joy_callback(self, msg):
        self.joy = msg
        if len(self.joy.axes)>0 or len(self.joy.buttons)>0 :
            self.joy_check=1
            if self.joy.buttons[1]==1:
                self.number = self.number+1
                if self.number>= self.robot_number+self.jackal_robot_number:
                    self.number = 0
            if self.joy.buttons[2]==1:
                self.together = self.together+1
            if self.joy.buttons[0]==1:
                self.hold = self.hold+1
            if self.joy.buttons[3]==1:
                self.all_stop = self.all_stop + 1

def input(rbt):
    if rbt.hold %2 == 1:
        print("Hold now, press Button[0] to control again \n")
    elif rbt.hold %2 == 0:
        if rbt.all_stop %2 == 1:
            vel_input = Twist()
            vel_input.linear.x = 0
            vel_input.angular.z = 0
            for i in range(rbt.robot_number):
                rbt.input_pub[i].publish(vel_input)
            print("All stop!")
            print("All stop!")
            print("All stop! \n")
            rbt.all_stop = 0
        elif rbt.together %2 == 1:
            vel_input = Twist()
            vel_input.linear.x = rbt.joy.axes[4] * rbt.max_lin_v
            vel_input.angular.z = rbt.joy.axes[0] * rbt.max_ang_v
            for i in range(rbt.robot_number+rbt.jackal_robot_number):
                rbt.input_pub[i].publish(vel_input)
            print("Altogether, Input : X: %.1f  Yaw: %.1f \n"%(vel_input.linear.x, vel_input.angular.z*180.0/np.pi))
        else:
            vel_input = Twist()
            vel_input.linear.x = rbt.joy.axes[4] * rbt.max_lin_v
            vel_input.angular.z = rbt.joy.axes[0] * rbt.max_ang_v
            rbt.input_pub[rbt.number].publish(vel_input)
            print("Robot <%d>, Input : X: %.1f  Yaw: %.1f \n"%(rbt.number+1, vel_input.linear.x, vel_input.angular.z*180.0/np.pi))
##############################################################################################

ctr = robot()
time.sleep(1) #wait 1 second to assure that all data comes in

''' main '''
if __name__ == '__main__':
    while 1:
        try:
            if ctr.joy_check==1:
                input(ctr)
                ctr.rate.sleep()
            else: 
                ctr.rate.sleep()
                pass
        except (rospy.ROSInterruptException, SystemExit, KeyboardInterrupt) :
            sys.exit(0)
        #except:
        #    print("exception")
        #    pass

